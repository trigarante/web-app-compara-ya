import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Poliza} from '../interfaces/poliza';
import {environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class PolizaService {

    constructor(private http: HttpClient) {
    }

    getAllRegistroByIdUsuario(idCliente: number): Observable<Array<Poliza>> {
        return this.http.get<Array<Poliza>>(environment.apiMark + 'v1/app-cliente/get-registro-cliente/' + idCliente);
    }
    getAllRegistroById(idPoliza: any): Observable<Poliza> {
        return this.http.get<Poliza>(environment.apiMark + 'v1/app-cliente/get-registro/' + idPoliza);
    }
  getDocument(archivo: string, idcliente: string, idregistro: string) {
    return this.http.get(`https://app.mark-43.net/v1/cliente/poliza/${archivo}`, {
      responseType: 'blob',
      headers: {
        idcliente: idcliente,
        idregistro: idregistro,
        idMarca: '2'
      }
    });
  }
}
