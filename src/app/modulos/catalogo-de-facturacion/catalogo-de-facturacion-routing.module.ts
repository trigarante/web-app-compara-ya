import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CatalogoDeFacturacionPageComponent} from './containers';

const routes: Routes = [
  {
    path: '',
    component: CatalogoDeFacturacionPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogoDeFacturacionRoutingModule { }
