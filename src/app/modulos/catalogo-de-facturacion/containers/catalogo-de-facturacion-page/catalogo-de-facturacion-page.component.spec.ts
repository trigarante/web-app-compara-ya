import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoDeFacturacionPageComponent } from './catalogo-de-facturacion-page.component';

describe('CatalogoDeFacturacionPageComponent', () => {
  let component: CatalogoDeFacturacionPageComponent;
  let fixture: ComponentFixture<CatalogoDeFacturacionPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CatalogoDeFacturacionPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoDeFacturacionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
