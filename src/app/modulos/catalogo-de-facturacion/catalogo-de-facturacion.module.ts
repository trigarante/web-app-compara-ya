import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogoDeFacturacionRoutingModule } from './catalogo-de-facturacion-routing.module';
import { CatalogoDeFacturacionComponent } from './components/catalogo-de-facturacion/catalogo-de-facturacion.component';
import { CatalogoDeFacturacionPageComponent } from './containers/catalogo-de-facturacion-page/catalogo-de-facturacion-page.component';
import {SharedModule} from '../../shared/shared.module';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';


@NgModule({
  declarations: [CatalogoDeFacturacionComponent, CatalogoDeFacturacionPageComponent],
  imports: [
    CommonModule,
    CatalogoDeFacturacionRoutingModule,
    SharedModule,
    MatToolbarModule,
    MatCardModule
  ]
})
export class CatalogoDeFacturacionModule { }
