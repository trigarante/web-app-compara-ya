import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import {DatosCliente, Poliza} from '../../../../@core/interfaces/poliza';
import {ClienteService} from '../../../../@core/services/cliente.service';
import {PolizaService} from '../../../../@core/services/poliza.service';
@Component({
  selector: 'app-detalle-registro',
  templateUrl: './detalle-registro.component.html',
  styleUrls: ['./detalle-registro.component.scss']
})
export class DetalleRegistroComponent implements OnInit {
  usuario;
  usuarioActivo: number;
  nombre: string;
  datoCliente: DatosCliente;
  poliza: Poliza;
  loaderCondition = false;
  constructor(private clienteService: ClienteService, private polizaService: PolizaService) {
    this.usuario = JSON.parse(localStorage.getItem('token'));
    this.poliza = JSON.parse(localStorage.getItem('itemPoliza'));
    this.datoCliente = JSON.parse(localStorage.getItem('itemPoliza'));
    this.usuarioActivo = this.usuario.id;
    setTimeout(() => {
      this.loaderCondition = true;
    }, 1000);
  }

  ngOnInit() {
    // this.getClienteById();
    // this.getPolizas();
  }
  // getClienteById() {
  //   this.clienteService.getCLienteById(this.usuarioActivo).subscribe(data => {
  //     this.nombre = data.nombre + ' ' + data.paterno + ' ' + data.materno;
  //   });
  // }
  // getPolizas() {
  //   this.polizaService.getAllRegistroById(this.usuarioActivo).subscribe(data => {
  //     this.datoCliente = JSON.parse(data.datos);
  //     data.fechaRegistro = moment(data.fechaRegistro).format('DD/MM/YYYY');
  //     data.fechaInicio = moment(data.fechaInicio).format('DD/MM/YYYY');
  //     this.poliza = data;
  //     this.loaderCondition = true;
  //   });
  // }
}
