import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AclaracionYSugerenciasPageComponent } from './aclaracion-y-sugerencias-page.component';

describe('AclaracionYSugerenciasPageComponent', () => {
  let component: AclaracionYSugerenciasPageComponent;
  let fixture: ComponentFixture<AclaracionYSugerenciasPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AclaracionYSugerenciasPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AclaracionYSugerenciasPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
