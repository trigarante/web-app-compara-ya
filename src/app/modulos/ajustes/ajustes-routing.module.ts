import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AjustesPageComponent} from './containers/ajustes-page/ajustes-page.component';

const routes: Routes = [
  {
    path: '',
    component: AjustesPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AjustesRoutingModule { }
