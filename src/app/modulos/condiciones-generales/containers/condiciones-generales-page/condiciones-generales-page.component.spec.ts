import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CondicionesGeneralesPageComponent } from './condiciones-generales-page.component';

describe('CondicionesGeneralesPageComponent', () => {
  let component: CondicionesGeneralesPageComponent;
  let fixture: ComponentFixture<CondicionesGeneralesPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CondicionesGeneralesPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CondicionesGeneralesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
