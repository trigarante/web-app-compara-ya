import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdentificacionOficialComponent } from './identificacion-oficial.component';

describe('IdentificacionOficialComponent', () => {
  let component: IdentificacionOficialComponent;
  let fixture: ComponentFixture<IdentificacionOficialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IdentificacionOficialComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentificacionOficialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
