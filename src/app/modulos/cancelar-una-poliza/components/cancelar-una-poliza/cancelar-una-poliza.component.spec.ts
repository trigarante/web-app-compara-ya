import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelarUnaPolizaComponent } from './cancelar-una-poliza.component';

describe('CancelarUnaPolizaComponent', () => {
  let component: CancelarUnaPolizaComponent;
  let fixture: ComponentFixture<CancelarUnaPolizaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CancelarUnaPolizaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelarUnaPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
