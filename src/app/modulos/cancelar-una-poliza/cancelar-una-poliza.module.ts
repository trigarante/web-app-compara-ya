import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CancelarUnaPolizaRoutingModule } from './cancelar-una-poliza-routing.module';
import { CancelarUnaPolizaComponent } from './components/cancelar-una-poliza/cancelar-una-poliza.component';
import { CancelarUnaPolizaPageComponent } from './containers/cancelar-una-poliza-page/cancelar-una-poliza-page.component';
import {SharedModule} from '../../shared/shared.module';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';


@NgModule({
  declarations: [CancelarUnaPolizaComponent, CancelarUnaPolizaPageComponent],
  imports: [
    CommonModule,
    CancelarUnaPolizaRoutingModule,
    SharedModule,
    MatToolbarModule,
    MatCardModule
  ]
})
export class CancelarUnaPolizaModule { }
