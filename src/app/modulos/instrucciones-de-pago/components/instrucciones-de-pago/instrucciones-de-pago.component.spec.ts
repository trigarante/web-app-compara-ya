import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstruccionesDePagoComponent } from './instrucciones-de-pago.component';

describe('InstruccionesDePagoComponent', () => {
  let component: InstruccionesDePagoComponent;
  let fixture: ComponentFixture<InstruccionesDePagoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstruccionesDePagoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstruccionesDePagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
