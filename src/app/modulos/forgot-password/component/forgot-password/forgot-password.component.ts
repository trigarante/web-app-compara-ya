import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import iziToast from "izitoast";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  hide = true;
  hide2 = true;

  constructor(private formBuilder: FormBuilder) {
  }

  formGroup: FormGroup;

  ngOnInit(): void {
    this.startBuildForm();
  }

  private startBuildForm(): void {
    this.formGroup = this.formBuilder.group({
      password: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}'),
        Validators.minLength(8), Validators.maxLength(32)]],
      confirmPassword: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}'),
        Validators.minLength(8), Validators.maxLength(32)]],
    });
  }

  throwErrorsPassword() {
    if (this.passwordField.value !== this.confirmPasswordField.value) {
      iziToast.error({
        title: 'Error',
        message: 'Las contraseñas no coinciden',
      });
    } else {
      iziToast.success({
        title: 'Contraseña Aprobada',
      });
    }
  }

  get passwordField(): AbstractControl {
    return this.formGroup.get('password');
  }

  get confirmPasswordField(): AbstractControl {
    return this.formGroup.get('confirmPassword');
  }

}
