import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef } from "@angular/material/dialog";
import { EmailService } from 'src/app/modulos/auth/services';

@Component({
  selector: 'app-mail-field',
  templateUrl: './mail-field.component.html',
  styleUrls: ['./mail-field.component.scss']
})
export class MailFieldComponent implements OnInit {
  loginForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private closeDialog: MatDialogRef<MailFieldComponent>,
    private emailService: EmailService
  ) {
    this.loginForm = this.fb.group({
      email: new FormControl('', Validators.compose([Validators.required, /*Validators.pattern(this.decimal)*/])),
      contrasenaApp: new FormControl('', Validators.compose([Validators.required, /*Validators.pattern(this.decimal)*/])),
      activo: 1,
      idMarcaEmpresa: 1
    });
  }

  ngOnInit(): void {
  }


  validateEmail() {

    let idCliente: number;

    if (this.loginForm.get('email').valid) {
      this.emailService.getInfoEmailRecoveryPassword(this.loginForm.get('email').value).subscribe({
        next: (dataResponse: any) => {
          // console.log('Data Response => ', dataResponse.id);
          idCliente = dataResponse.id;
        },
        error: (error) => {
          console.error('Error al validar el correo electrónico');
        },
        complete: () => {
          this.sendEmail(idCliente);
        }
      });
    } else {
      console.log('Se requiere el campo');

    }


    // this.closeDialog.close();
  }

  private sendEmail(clientID: number) {
    console.log('Cliente id enviado => ', clientID);

    this.emailService.sendEmailAutomatic(clientID).subscribe({
      next: (dataResponse) => {
        console.log('Data Response Send Email => ', dataResponse);
      },
      error: (error) => {
        console.error('Error al enviar el correo => ', error);
      },
      complete: () => {
        console.log('Se envió correctamente');
      }
    });
  }

}
