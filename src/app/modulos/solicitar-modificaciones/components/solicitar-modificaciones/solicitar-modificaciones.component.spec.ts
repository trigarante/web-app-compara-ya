import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitarModificacionesComponent } from './solicitar-modificaciones.component';

describe('DatosPersonalesComponent', () => {
  let component: SolicitarModificacionesComponent;
  let fixture: ComponentFixture<SolicitarModificacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitarModificacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitarModificacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
