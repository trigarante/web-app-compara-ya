import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MailFieldComponent } from 'src/app/modulos/forgot-password/component/mail-field/mail-field.component';
import { AuthService } from '../../services';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  loginForm: FormGroup;
  hide = true;
  result = '';
  validate = false;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private dialog: MatDialog
  ) {
    this.loginForm = this.fb.group({
      email: new FormControl('', Validators.compose([Validators.required, /*Validators.pattern(this.decimal)*/])),
      contrasenaApp: new FormControl('', Validators.compose([Validators.required, /*Validators.pattern(this.decimal)*/])),
      activo: 1,
      idMarcaEmpresa: 2
    });
  }
  ngOnInit() {
  }

  iniciarSesion(): void {
    if (this.loginForm.invalid) {
      return;
    }
    this.authService.auth(this.loginForm.value).then(result => {
      localStorage.setItem('userEmail', this.emailField.value);
    }).catch((result) => {
      this.result = 'Parece que tus datos son incorrectos. Por favor intentalo nuevamente.';
    });
  }

  get contrasenaAppField(): AbstractControl {
    return this.loginForm.get('contrasenaApp');
  }

  get emailField(): AbstractControl {
    return this.loginForm.get('email');
  }

  forgotPasswordRedirect() {
    this.dialog.open(MailFieldComponent);
  }

}
